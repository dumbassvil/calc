package com.example.user.calc

/**
 * Created by User on 22.11.2017.
 */
//возведение в квадрат
//возведение в степень
//процент от числа(например 10% от 5)
class CalcSubUtils():CalcUtils(){
    val defaulPerecent = 100
    fun squareOperation (a:Int):Int{
        return a*a
    }

    fun powerOperation (a: Int, b: Int):Int{
        var result = a
        var iterator = 1
        while (iterator < b){
            result *= a
            iterator++
        }
        return result
    }

    fun precentOperation (number: Int, perecent: Int):Float{

        return number.toFloat()*perecent.toFloat()/defaulPerecent
    }
}
package com.example.user.calc

/**
 * Created by User on 22.11.2017.
 */
open class CalcUtils(){

    val sayHi = "hi"
    protected val sayElse = "i have an armor"
    private val sayMore = "i'm god"

    fun plusOperation(a:Int, b:Int):Int{
        return a+b
    }

    fun minusOperation(a:Int, b:Int):Int{
        return a-b
    }

    fun divisionOperation (a: Int, b: Int):Int{
        var result = 0
        if(a!=0){
            if (b!=0){
                result = a.div(b)
            }
        }

        return result
    }

    fun multiplicationOperation (a: Int, b: Int):Int{
        return a*b
    }

    fun saySomething(word:String){
        print(word)
    }
    fun generateButtons(buttonTexts:Array<String>):ArrayList<MyButton>{
        val createdButtons = arrayListOf<MyButton>()
        buttonTexts.forEach {
            createdButtons.add(MyButton(it))
        }
        print(createdButtons)
        return createdButtons
    }
}

data class MyButton(val text:String)
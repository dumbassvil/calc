package com.example.user.calc

import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.util.Log

class MainActivity : AppCompatActivity() {
    val calcUtils = CalcSubUtils()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        Log.d("plus operation","${calcUtils.plusOperation(10,-2)}")
        Log.d("minus","${calcUtils.minusOperation(-8,3)}")
        Log.d("multiple", "${calcUtils.multiplicationOperation(2,0)}")
        Log.d("div 1","${calcUtils.divisionOperation(10,0)}")
        Log.d("div 2","${calcUtils.divisionOperation(0,5)}")
        Log.d("sqr 1","${calcUtils.squareOperation(256)}")
        Log.d("power ","${calcUtils.powerOperation(2,10)}")
        Log.d("power 2","${calcUtils.powerOperation(-3,3)}")
        Log.d("percent","${calcUtils.precentOperation(1,25)}")

        print(0)
    }
}
